# StoryShare

Storyshare allows you to broadcast Stories in the web browser. 
Possibility to segment and analyze your stories. 
Integration with other services (CRM ...) gives you incredible power.
We develop a web platform to create unique and interactive stories. 🚀


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

[***Node JS 12.X***](https://nodejs.org/en/)
[***NPM***](https://www.npmjs.com/get-npm)

### Installing

*Dev environment setup :*

  ***API*** 

 
```
cd api
```
```
nest build --watch --webpack
```

In another terminal window
```
cd api
```
```
npm run start
```

***Front***

In a new terminal window
```
cd front
```
```
npm install
```
```
npm start
```

## Running the tests

In progress

## Deployment

In progress

## Built With

* [Angular](https://angular.io/) - Front end framework
* [Nest JS](https://docs.nestjs.com/) - Back end framework



## Versioning

Last version : V 0.0.1

## Authors

* **Antoine BIAMOURET** - *Full-stack Developer*
* **Quentin LABEYRIE** - *Full-stack Developer*
