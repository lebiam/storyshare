import { Type } from './Type.enum';
import { Component } from './Component';
export interface Story {
    id:number;
    source: String;
    type: Type;
    duration: number;
    progression: number;
    component?: Component;
}
