import { Story } from './Story';

export class Workspace {
    id: number;
    name: string;
    stories: Story[];
}
