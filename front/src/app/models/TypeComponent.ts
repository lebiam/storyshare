export enum TypeComponent {
    input = "input",
    countdown = "countdown",
    button = "button",
    multiple = "multiple",
    slider = "slider",
    utils = "utils"
}