import { TypeComponent } from './TypeComponent';

export interface Component {
    id: number;
	positionX?: number;
	positionY?: number;
	rotation?: number;
	type : TypeComponent;
	data : Object;
}