export interface AnswerMultipleChoice {
    id: number;
    answer: String;
}