export interface Slider {
    title: number;
	emoji: string;
	colorBar: string;
	colorBackground: string;
}