import { AnswerMultipleChoice } from './AnswerMultipleChoice';

export interface QuestionMultipleChoice {
    id: number;
    question: String;
    answers: AnswerMultipleChoice[];
}
