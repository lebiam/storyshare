import { CreateComponent } from './web-app/create/create.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayerVideoComponent } from './player-video/player-video.component';
import { WebAppComponent } from './web-app/web-app.component';
import { AuthGuard } from './auth/auth.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './auth/interceptor.service';


const routes: Routes = [
  {path: 'create' , component: CreateComponent},
  {path: 'play/:id' , component: PlayerVideoComponent},
  {path: 'user/profile' , component: UserProfileComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: '/create', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: InterceptorService,
    //   multi: true
    // }
  ]
})
export class AppRoutingModule { }
