import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerVideoService {

  constructor(private http: HttpClient) { }

  getStories$(id: number): Observable<any> {
    return this.http.get('http://localhost:3001/play/445');
  }

}
