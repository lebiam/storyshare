import { Story } from 'src/app/models/Story';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progress-wrapper-player',
  templateUrl: './progress-wrapper-player.component.html',
  styleUrls: ['./progress-wrapper-player.component.css']
})
export class ProgressWrapperPlayerComponent implements OnInit {
  @Input() stories: Story[];

  constructor() { }

  ngOnInit() {
  }

}
