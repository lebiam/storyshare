import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-powered-by',
  templateUrl: './powered-by.component.html',
  styleUrls: ['./powered-by.component.css']
})
export class PoweredByComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  goToHomepage(){
    console.log("GotoHomepage");
    window.open("http://localhost:3000/", "_blank");
}

}
