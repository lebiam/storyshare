import { DisplayStoryDto } from './../../../../api/src/display-storyshare/dto/display-storyshare.dto';
import { PlayerVideoService } from './services/player-video.service';
import { Component, OnInit } from '@angular/core';
import { Story } from '../models/Story';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-player-video',
  templateUrl: './player-video.component.html',
  styleUrls: ['./player-video.component.css']
})
export class PlayerVideoComponent implements OnInit {
  stories : Story[];
  data : DisplayStoryDto;
  storiesChange(updateStories: Story[]){
    this.stories = updateStories;
  }

  constructor(private playerVideoService: PlayerVideoService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    this.playerVideoService.getStories$(787).subscribe((data: DisplayStoryDto) => {
      this.stories = data.workspace.stories;
      this.data = data;
    });
  }
}
