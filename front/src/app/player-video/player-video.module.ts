import { PoweredByComponent } from './components/powered-by/powered-by.component';
import { HeaderComponent } from './components/header/header.component';
import { PlayerVideoService } from './services/player-video.service';
import { LongPressDirective } from './../directives/LongPress.directive';
import { ProgressWrapperPlayerComponent } from './components/progress-wrapper-player/progress-wrapper-player.component';
import { ContentPlayerComponent } from './components/content-player/content-player.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerVideoComponent } from './player-video.component';
import { ProgressPlayerComponent } from './components/progress-wrapper-player/progress-player/progress-player.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [PlayerVideoService],
  declarations: [
    PlayerVideoComponent,
    ContentPlayerComponent,
    ProgressWrapperPlayerComponent,
    ProgressPlayerComponent,
    LongPressDirective,
    HeaderComponent, 
    PoweredByComponent
  ],
  exports:[
    PlayerVideoComponent,
  ],
})
export class PlayerVideoModule { }

