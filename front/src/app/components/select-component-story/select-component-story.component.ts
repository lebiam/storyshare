import { QuestionMultipleChoice } from './../../models/QuestionMultipleChoice';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-component-story',
  templateUrl: './select-component-story.component.html',
  styleUrls: ['./select-component-story.component.css']
})
export class SelectComponentStoryComponent implements OnInit {

  titleSlider : string = "Rate your experience !";
  colorBar: string = "#FF3465";
  colorFontCountdown: string = "#F5F5F5";
  titleCountdown: string = "Available soon !";
  titleButton: string = "ORDER NEW";
  titleInput: string = "Give us a feedback !";

  question: QuestionMultipleChoice = {
    id : 555,
    question: "Your question ?",
    answers: [
      {
        id: 8545,
        answer: "Yes"
      },
      {
        id: 856,
        answer: "No"
      }
    ]
  }
  constructor() { }

  ngOnInit() {
  }

}
