import { Type } from './../../models/Type.enum';
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Story } from 'src/app/models/Story';
import { ButtonEditStoryComponent } from './button-edit-story/button-edit-story.component';
import { SelectEditStoryComponent } from './select-edit-story.component';

var stories: any = [
  {
    name : "Create",
    active : true
  },
  {
    name : "Connect",
    active : false
  },
  {
    name : "Share",
    active : false
  },
  {
    name : "Results",
    active : false
  },
];

storiesOf('SelectEditStory', module)
.addDecorator(
    moduleMetadata({
      declarations: [ButtonEditStoryComponent,SelectEditStoryComponent],
      imports: [FontAwesomeModule]
    })
  )
  .add('default', () => {
    return {
      template: `<app-select-edit-story [stories]="stories"></app-select-edit-story>`,
      props: {
        stories
      },
    };
  })
;
