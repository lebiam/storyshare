import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { ButtonEditStoryComponent } from './button-edit-story.component';

var active = true;
var inactive = false;
var isAddBtn = true;
var isNotAddBtn = false;

storiesOf('ButtonEditStory', module)
.addDecorator(
    moduleMetadata({
      declarations: [ButtonEditStoryComponent],
      imports: [FontAwesomeModule]
    })
  )
  .add('active', () => {
    return {
      template: `<app-button-edit-story [isActive]="active" [isAddBtn]="isNotAddBtn" ></app-button-edit-story>`,
      props: {
        active,
        isNotAddBtn
      },
    };
  })
  .add('inactive', () => {
    return {
      template: `<app-button-edit-story [isActive]="inactive" [isAddBtn]="isNotAddBtn"></app-button-edit-story>`,
      props: {
        inactive,
        isNotAddBtn
      },
    };
  })
  .add('add', () => {
    return {
      template: `<app-button-edit-story [isActive]="inactive" [isAddBtn]="isAddBtn"></app-button-edit-story>`,
      props: {
        isAddBtn,
        isNotAddBtn
      },
    };
  })
;