import { Component, OnInit, Input } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-button-edit-story',
  templateUrl: './button-edit-story.component.html',
  styleUrls: ['./button-edit-story.component.css']
})
export class ButtonEditStoryComponent implements OnInit {

  faPlus = faPlus;

  @Input() isActive:  boolean;
  @Input() isAddBtn: boolean;
  @Input() number: number;

  constructor() { }

  ngOnInit() {
  }

}
