import { Story } from './../../models/Story';
import { Component, OnInit, Input } from '@angular/core';
import { Type } from 'src/app/models/Type.enum';

@Component({
  selector: 'app-select-edit-story',
  templateUrl: './select-edit-story.component.html',
  styleUrls: ['./select-edit-story.component.css']
})
export class SelectEditStoryComponent implements OnInit {

  @Input() stories: Story[];
  constructor() { }

  ngOnInit() {
  }

  addStory(){
    this.stories.push({
      id: 456,
      source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
      type : Type.Video,
      duration : 6000,
      progression: 0}
    )
  }

}
