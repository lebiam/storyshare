import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigSliderStoryshareComponent } from './config-slider-storyshare.component';

describe('ConfigSliderStoryshareComponent', () => {
  let component: ConfigSliderStoryshareComponent;
  let fixture: ComponentFixture<ConfigSliderStoryshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigSliderStoryshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigSliderStoryshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
