import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-config-slider-storyshare',
  templateUrl: './config-slider-storyshare.component.html',
  styleUrls: ['./config-slider-storyshare.component.css']
})
export class ConfigSliderStoryshareComponent implements OnInit {

  @Input() component_data: String;

  constructor() { }

  ngOnInit() {
  }

}
