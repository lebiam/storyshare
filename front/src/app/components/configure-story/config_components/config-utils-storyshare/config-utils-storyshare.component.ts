import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-config-utils-storyshare',
  templateUrl: './config-utils-storyshare.component.html',
  styleUrls: ['./config-utils-storyshare.component.less']
})
export class ConfigUtilsStoryshareComponent implements OnInit {

  @Input() component_data: String;

  constructor() { }

  ngOnInit() {
  }

}
