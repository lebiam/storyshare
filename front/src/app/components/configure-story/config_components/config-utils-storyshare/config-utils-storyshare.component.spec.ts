import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigUtilsStoryshareComponent } from './config-utils-storyshare.component';

describe('ConfigUtilsStoryshareComponent', () => {
  let component: ConfigUtilsStoryshareComponent;
  let fixture: ComponentFixture<ConfigUtilsStoryshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigUtilsStoryshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigUtilsStoryshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
