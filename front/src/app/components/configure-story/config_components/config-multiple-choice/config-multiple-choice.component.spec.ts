import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigMultipleChoiceComponent } from './config-multiple-choice.component';

describe('ConfigMultipleChoiceComponent', () => {
  let component: ConfigMultipleChoiceComponent;
  let fixture: ComponentFixture<ConfigMultipleChoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigMultipleChoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigMultipleChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
