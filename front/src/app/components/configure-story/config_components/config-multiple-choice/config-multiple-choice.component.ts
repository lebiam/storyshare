import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-config-multiple-choice',
  templateUrl: './config-multiple-choice.component.html',
  styleUrls: ['./config-multiple-choice.component.less']
})
export class ConfigMultipleChoiceComponent implements OnInit {

  @Input() component_data: String;

  constructor() { }

  ngOnInit() {
  }

}
