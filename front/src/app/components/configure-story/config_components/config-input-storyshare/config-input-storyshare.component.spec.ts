import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigInputStoryshareComponent } from './config-input-storyshare.component';

describe('ConfigInputStoryshareComponent', () => {
  let component: ConfigInputStoryshareComponent;
  let fixture: ComponentFixture<ConfigInputStoryshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigInputStoryshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigInputStoryshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
