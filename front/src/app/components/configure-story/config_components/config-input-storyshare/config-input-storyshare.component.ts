import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-config-input-storyshare',
  templateUrl: './config-input-storyshare.component.html',
  styleUrls: ['./config-input-storyshare.component.less']
})
export class ConfigInputStoryshareComponent implements OnInit {

  @Input() component_data: String;

  constructor() { }

  ngOnInit() {
  }

}
