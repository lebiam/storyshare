import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigButtonStoryshareComponent } from './config-button-storyshare.component';

describe('ConfigButtonStoryshareComponent', () => {
  let component: ConfigButtonStoryshareComponent;
  let fixture: ComponentFixture<ConfigButtonStoryshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigButtonStoryshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigButtonStoryshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
