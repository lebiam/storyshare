import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-config-button-storyshare',
  templateUrl: './config-button-storyshare.component.html',
  styleUrls: ['./config-button-storyshare.component.css']
})
export class ConfigButtonStoryshareComponent implements OnInit {

  @Input() component_data: String;

  constructor() { }

  ngOnInit() {
  }

}
