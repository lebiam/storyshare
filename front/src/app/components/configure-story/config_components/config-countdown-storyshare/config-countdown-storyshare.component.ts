import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-config-countdown-storyshare',
  templateUrl: './config-countdown-storyshare.component.html',
  styleUrls: ['./config-countdown-storyshare.component.less']
})
export class ConfigCountdownStoryshareComponent implements OnInit {

  @Input() component_data: String;

  constructor() { }

  ngOnInit() {
  }

}
