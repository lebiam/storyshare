import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigCountdownStoryshareComponent } from './config-countdown-storyshare.component';

describe('ConfigCountdownStoryshareComponent', () => {
  let component: ConfigCountdownStoryshareComponent;
  let fixture: ComponentFixture<ConfigCountdownStoryshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigCountdownStoryshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigCountdownStoryshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
