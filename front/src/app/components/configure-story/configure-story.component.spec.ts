import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureStoryComponent } from './configure-story.component';

describe('ConfigureStoryComponent', () => {
  let component: ConfigureStoryComponent;
  let fixture: ComponentFixture<ConfigureStoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureStoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
