import { Component, OnInit } from '@angular/core';
import { TypeComponent } from 'src/app/models/TypeComponent';

@Component({
  selector: 'app-configure-story',
  templateUrl: './configure-story.component.html',
  styleUrls: ['./configure-story.component.css']
})
export class ConfigureStoryComponent implements OnInit {

  buttonSaveContext : any = {
    titleButtonSave : "SAVE",
    fontColor : "#FFFFFF",
    colorBackgroundButtonSave : "#522CBB"
  };

  component_data : any = {
    id: 1,
    positionX : 50,
    positionY : 20,
    rotation : 90,
    type : "slider",
    data : {
      title : "Rate your experience",
      emojiId : 1,
      colorBackground : "#522CBB"
    }
  };
  
  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }
    return value;
  }

  constructor() { }

  ngOnInit() {
  }

}
