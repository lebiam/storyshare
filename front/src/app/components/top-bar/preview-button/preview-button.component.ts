import { Component, OnInit } from '@angular/core';
import { faEye } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-preview-button',
  templateUrl: './preview-button.component.html',
  styleUrls: ['./preview-button.component.css']
})
export class PreviewButtonComponent implements OnInit {

  faEye = faEye;
  constructor() { }

  ngOnInit() {
  }

}
