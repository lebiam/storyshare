import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { PreviewButtonComponent } from './preview-button.component';


storiesOf('PreviewButton', module)
.addDecorator(
    moduleMetadata({
      declarations: [PreviewButtonComponent],
      imports:[FontAwesomeModule]
    })
  )
  .add('default', () => {
    return {
      template: `<app-preview-button></app-preview-button>`,
    };
  })
;