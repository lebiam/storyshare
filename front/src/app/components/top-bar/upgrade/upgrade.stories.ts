import { UpgradeComponent } from './upgrade.component';
import { storiesOf, moduleMetadata } from '@storybook/angular';

var nameActive : string = "Ma superbe storie";

storiesOf('Upgrade', module)
.addDecorator(
    moduleMetadata({
      declarations: [UpgradeComponent],
    })
  )
  .add('default', () => {
    return {
      template: `<app-upgrade></app-upgrade>`,
      props: {
        nameActive,
      },
    };
  })
;