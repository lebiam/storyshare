import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  firstCharacter: String;

  @Input()
  set name(val: String) {
    this.firstCharacter = val.charAt(0).toUpperCase();
  }
  constructor(public auth: AuthService) {
  }

  ngOnInit() {
  }

}
