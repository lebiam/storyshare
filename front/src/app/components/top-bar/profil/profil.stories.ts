import { ProfilComponent } from './profil.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { storiesOf, moduleMetadata } from '@storybook/angular';

var nameProfil: string = "Antoine Biamouret";

storiesOf('Profil', module)
.addDecorator(
    moduleMetadata({
      declarations: [ProfilComponent],
      imports:[FontAwesomeModule]
    })
  )
  .add('default', () => {
    return {
      template: `<app-profil [name]="nameProfil"></app-profil>`,
      props: {
        nameProfil,
      },
    };
  })
;
