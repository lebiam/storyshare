import { NameWorkspaceComponent } from './name-workspace.component';
import { storiesOf, moduleMetadata } from '@storybook/angular';

var nameActive : string = "Ma superbe storie";
var nameInactive : string = "Story de ouf !!!";

storiesOf('NameWorkspace', module)
.addDecorator(
    moduleMetadata({
      declarations: [NameWorkspaceComponent],
    })
  )
  .add('default', () => {
    return {
      template: `<app-name-workspace [name]="nameActive"></app-name-workspace>`,
      props: {
        nameActive,
      },
    };
  })
;