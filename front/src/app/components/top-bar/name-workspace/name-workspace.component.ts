import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-name-workspace',
  templateUrl: './name-workspace.component.html',
  styleUrls: ['./name-workspace.component.css']
})
export class NameWorkspaceComponent implements OnInit {

  @Input() name: string;
  
  constructor() { }

  ngOnInit() {
  }

}
