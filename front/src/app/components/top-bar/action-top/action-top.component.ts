import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActionTop } from 'src/app/models/ActionTop';

@Component({
  selector: 'app-action-top',
  templateUrl: './action-top.component.html',
  styleUrls: ['./action-top.component.css']
})
export class ActionTopComponent implements OnInit {

  @Input() action: ActionTop;
  
  constructor() { }

  ngOnInit() {
  }

}
