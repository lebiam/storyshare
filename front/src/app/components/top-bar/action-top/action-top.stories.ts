import { storiesOf, moduleMetadata } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { ActionTopComponent } from './action-top.component';

export const actionActive = {
    name: 'Create',
    active: true
};

export const actionInactive = {
    name: 'Connect',
    active: false
};

storiesOf('ActionTop', module)
.addDecorator(
    moduleMetadata({
      declarations: [ActionTopComponent],
    })
  )
  .add('active', () => {
    return {
      template: `<app-action-top [action]="actionActive"></app-action-top>`,
      props: {
        actionActive,
      },
    };
  })
  .add('inactive', () => {
    return {
      template: `<app-action-top [action]="actionInactive"></app-action-top>`,
      props: {
        actionInactive,
      },
    };
  })
;