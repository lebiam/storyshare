import { ActionTop } from 'src/app/models/ActionTop';
import { TopBarComponent } from './top-bar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { ActionTopComponent } from './action-top/action-top.component';
import { NameWorkspaceComponent } from './name-workspace/name-workspace.component';
import { PreviewButtonComponent } from './preview-button/preview-button.component';
import { ProfilComponent } from './profil/profil.component';
import { BackComponent } from './back/back.component';

var listActionTop: Array<ActionTop> = [
    {
      name : "Create",
      active : true
    },
    {
      name : "Connect",
      active : false
    },
    {
      name : "Share",
      active : false
    },
    {
      name : "Results",
      active : false
    },
  ];

  var nameWorkspace: String = "Ma première story";
  var nameUser: String = "Antoine";

storiesOf('TopBar', module)
.addDecorator(
    moduleMetadata({
      declarations: [ActionTopComponent,
        NameWorkspaceComponent,
        PreviewButtonComponent,
        ProfilComponent,
        TopBarComponent,
        BackComponent],
      imports:[FontAwesomeModule]
    })
  )
  .add('default', () => {
    return {
      template: `<app-top-bar [nameUser]="nameUser" [nameWorkspace]="nameWorkspace" [listActionTop]="listActionTop"></app-top-bar>`,
      props:{
        nameWorkspace,
        nameUser,
        listActionTop
      }
    };
  })
;