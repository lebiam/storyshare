/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { InputStoryshareComponent } from './input-storyshare.component';

describe('InputStoryshareComponent', () => {
  let component: InputStoryshareComponent;
  let fixture: ComponentFixture<InputStoryshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputStoryshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputStoryshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
