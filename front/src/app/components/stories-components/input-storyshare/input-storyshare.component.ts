import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input-storyshare',
  templateUrl: './input-storyshare.component.html',
  styleUrls: ['./input-storyshare.component.css']
})
export class InputStoryshareComponent implements OnInit {

  @Input() title;
  constructor() { }

  ngOnInit() {
  }

}
