import { QuestionMultipleChoice } from './../../../models/QuestionMultipleChoice';
import { Component, OnInit, Input } from '@angular/core';
import { stringify } from 'querystring';

@Component({
  selector: 'app-multiple-choice',
  templateUrl: './multiple-choice.component.html',
  styleUrls: ['./multiple-choice.component.css']
})
export class MultipleChoiceComponent implements OnInit {

  @Input() question: QuestionMultipleChoice;
  
  constructor() { }

  ngOnInit() {
  }
}
