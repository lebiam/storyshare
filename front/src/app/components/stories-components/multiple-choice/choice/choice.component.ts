import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-choice',
  templateUrl: './choice.component.html',
  styleUrls: ['./choice.component.css']
})
export class ChoiceComponent implements OnInit {
  
  letterAnswer: String;
  @Input() answer: String;
  @Input()
  set position(val: Number) {
    this.letterAnswer = this.getLetterAnswerByPosition(val);
  }
  
  constructor() { }

  ngOnInit() {
  }

  getLetterAnswerByPosition(position: Number): String {
    switch(position) { 
      case 0: { 
         return "A";
      } 
      case 1: { 
         return "B";
      }
      case 2: { 
        return "C";
     } 
      default: { 
         return "";
      } 
   }; 
  }

}
