import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button-storyshare',
  templateUrl: './button-storyshare.component.html',
  styleUrls: ['./button-storyshare.component.css']
})
export class ButtonStoryshareComponent implements OnInit {

  @Input() title: string;
  @Input() fontColor: string;
  @Input() backgroundColor: string; 
  
  constructor() { }

  ngOnInit() {
  }

}
