import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.css']
})
export class CountdownComponent implements OnInit {

  @Input() time: number;
  @Input() colorBackground: string;
  @Input() colorFont: string;

  constructor() { }

  ngOnInit() {
  }

}
