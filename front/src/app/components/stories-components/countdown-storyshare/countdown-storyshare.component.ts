import { Component, OnInit, Input } from '@angular/core';
import { Observable, interval } from 'rxjs';


@Component({
  selector: 'app-countdown-storyshare',
  templateUrl: './countdown-storyshare.component.html',
  styleUrls: ['./countdown-storyshare.component.css']
})
export class CountdownStoryshareComponent implements OnInit {

  _trialEndsAt;
  _diff: number;
  _days: number;
  _hours: number;
  _minutes: number;
  _seconds: number;

  @Input() colorBackground: string;
  @Input() colorFont: string;
  @Input() title :string;

  constructor() { }

  ngOnInit() {
    const intervalTimer = interval(1000);
    this._trialEndsAt = "2020-01-29";
    intervalTimer.subscribe((x) => {
      this._diff = Date.parse(this._trialEndsAt) - Date.parse(new Date().toString());
      this._days = this.getDays(this._diff);
      this._hours = this.getHours(this._diff);
      this._minutes = this.getMinutes(this._diff);
      this._seconds = this.getSeconds(this._diff);
    });
  }

  getDays(t) {
    var result = Math.floor(t / (1000 * 60 * 60 * 24));
    return result > 0 ? result : 0;
  }

  getHours(t) {
    var result = Math.floor((t / (1000 * 60 * 60)) % 24);
    return result > 0 ? result : 0;
  }

  getMinutes(t) {
    var result = Math.floor((t / 1000 / 60) % 60);
    return result > 0 ? result : 0;
  }

  getSeconds(t) {
    var result = Math.floor((t / 1000) % 60);
    return result > 0 ? result : 0;
  }
}
