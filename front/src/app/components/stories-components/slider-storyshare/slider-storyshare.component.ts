import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slider-storyshare',
  templateUrl: './slider-storyshare.component.html',
  styleUrls: ['./slider-storyshare.component.css']
})
export class SliderStoryshareComponent implements OnInit {

  @Input() title : string;
  @Input() colorBar: string;

  constructor() { }

  ngOnInit() {
  }

}
