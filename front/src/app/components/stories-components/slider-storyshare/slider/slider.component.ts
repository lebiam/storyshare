import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery'

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  backgroundGradient: String;
  colorBarLower: String;

  @Input()
  set colorBar(val: String) {
    this.colorBarLower = val;
    this.backgroundGradient = "linear-gradient(to right, "+ val +" 0%, " + val + " 50%, #B5C0C4 50%, #B5C0C4 100%)"
  }
  
  
  jQuery = function(colorBarLower){
    $( '.slider-container input' ).on( 'input', function() {
      $( this ).css( 'background', 'linear-gradient(to right, ' + colorBarLower +' 0%, '+ colorBarLower +' '+ this.value +'%, #B5C0C4 ' + this.value + '%, #B5C0C4 100%)' );
    } );
  }
 

  constructor() { }

  ngOnInit() {
    this.jQuery(this.colorBarLower);
  }

}
