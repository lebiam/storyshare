import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { SliderComponent } from './slider.component';

var colorBar: string = "#FF3465";
storiesOf('Slider', module)
.addDecorator(
    moduleMetadata({
      declarations: [SliderComponent],
      imports: [FontAwesomeModule]
    })
  )
  .add('default', () => {
    return {
      template: `<app-slider [colorBar]="colorBar"></app-slider>`,
      props: {
        colorBar
      },
    };
  })
;