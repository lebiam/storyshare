import { ShareComponent } from './web-app/share/share.component';
import { ResultsComponent } from './web-app/results/results.component';
import { CreateComponent } from './web-app/create/create.component';
import { ConnectComponent } from './web-app/connect/connect.component';
import { InputStoryshareComponent } from './components/stories-components/input-storyshare/input-storyshare.component';
import { MultipleChoiceComponent } from './components/stories-components/multiple-choice/multiple-choice.component';
import { ChoiceComponent } from './components/stories-components/multiple-choice/choice/choice.component';
import { ButtonStoryshareComponent } from './components/stories-components/button-storyshare/button-storyshare.component';
import { SliderStoryshareComponent } from './components/stories-components/slider-storyshare/slider-storyshare.component';
import { TitleComponent } from './components/stories-components/utils-storyshare/title/title.component';
import { CountdownComponent } from './components/stories-components/countdown-storyshare/countdown/countdown.component';
import { SliderComponent } from './components/stories-components/slider-storyshare/slider/slider.component';
import { ButtonEditStoryComponent } from './components/select-edit-story/button-edit-story/button-edit-story.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatTooltipModule} from '@angular/material/tooltip';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ActionTopComponent } from './components/top-bar/action-top/action-top.component';
import { NameWorkspaceComponent } from './components/top-bar/name-workspace/name-workspace.component';
import { PreviewButtonComponent } from './components/top-bar/preview-button/preview-button.component';
import { ProfilComponent } from './components/top-bar/profil/profil.component';
import { BackComponent } from './components/top-bar/back/back.component';
import { SelectEditStoryComponent } from './components/select-edit-story/select-edit-story.component';
import { CountdownStoryshareComponent } from './components/stories-components/countdown-storyshare/countdown-storyshare.component';
import { SelectComponentStoryComponent } from './components/select-component-story/select-component-story.component';
import { InputComponent } from './components/stories-components/input-storyshare/input/input.component';
import { PlayerVideoModule } from './player-video/player-video.module';
import { ConfigureStoryComponent } from './components/configure-story/configure-story.component';
import { ConfigSliderStoryshareComponent } from './components/configure-story/config_components/config-slider-storyshare/config-slider-storyshare.component';
import { ConfigInputStoryshareComponent } from './components/configure-story/config_components/config-input-storyshare/config-input-storyshare.component';
import { ConfigUtilsStoryshareComponent } from './components/configure-story/config_components/config-utils-storyshare/config-utils-storyshare.component';
import { ConfigButtonStoryshareComponent } from './components/configure-story/config_components/config-button-storyshare/config-button-storyshare.component';
import { ConfigCountdownStoryshareComponent } from './components/configure-story/config_components/config-countdown-storyshare/config-countdown-storyshare.component';
import { ConfigMultipleChoiceComponent } from './components/configure-story/config_components/config-multiple-choice/config-multiple-choice.component';
import {MatSliderModule} from '@angular/material/slider';
import { FormsModule } from '@angular/forms';
import { WebAppComponent } from './web-app/web-app.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import {MatMenuModule} from '@angular/material/menu';
import { AuthService } from './auth/auth.service';



@NgModule({
   declarations: [
      AppComponent,
      ActionTopComponent,
      NameWorkspaceComponent,
      PreviewButtonComponent,
      ProfilComponent,
      TopBarComponent,
      BackComponent,
      SelectEditStoryComponent,
      ButtonEditStoryComponent,
      SliderComponent,
      SliderStoryshareComponent,
      CountdownComponent,
      CountdownStoryshareComponent,
      TitleComponent,
      SelectComponentStoryComponent,
      ButtonStoryshareComponent,
      ChoiceComponent,
      MultipleChoiceComponent,
      InputStoryshareComponent,
      InputComponent,
      ConfigureStoryComponent,
      ConfigSliderStoryshareComponent,
      ConfigInputStoryshareComponent,
      ConfigUtilsStoryshareComponent,
      ConfigButtonStoryshareComponent,
      ConfigCountdownStoryshareComponent,
      ConfigMultipleChoiceComponent,
      WebAppComponent,
      UserProfileComponent,
      ConnectComponent,
      CreateComponent,
      ResultsComponent,
      ShareComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      MatTooltipModule,
      MatSliderModule,
      FontAwesomeModule,
      PlayerVideoModule,
      FormsModule, 
      MatMenuModule
   ],
   providers: [AuthService],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
