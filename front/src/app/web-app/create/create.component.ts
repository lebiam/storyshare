import { Component, OnInit } from '@angular/core';
import { Type } from 'src/app/models/Type.enum';
import { Story } from 'src/app/models/Story';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  stories : Story[] = [
    {
      id: 456,
      source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
      type : Type.Video,
      duration : 6000,
      progression: 0
    },
    {
      id: 456,
      source : "https://picsum.photos/id/124/1080/1920",
      type : Type.Image,
      duration : 3000,
      progression: 0
    },
    {
      id: 456,
      source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
      type : Type.Video,
      duration : 6000,
      progression: 0
    },
    {
      id: 456,
      source : "https://picsum.photos/id/124/1080/1920",
      type : Type.Image,
      duration : 3000,
      progression: 0
    },
    {
      id: 456,
      source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
      type : Type.Video,
      duration : 6000,
      progression: 0
    },
    {
      id: 456,
      source : "https://picsum.photos/id/124/1080/1920",
      type : Type.Image,
      duration : 3000,
      progression: 0
    },
    {
      id: 456,
      source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
      type : Type.Video,
      duration : 6000,
      progression: 0
    },
    {
      id: 456,
      source : "https://picsum.photos/id/124/1080/1920",
      type : Type.Image,
      duration : 3000,
      progression: 0
    }
  ];
}
