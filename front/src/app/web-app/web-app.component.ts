import { Type } from 'src/app/models/Type.enum';
import { Component, OnInit } from '@angular/core';
import { Story } from '../models/Story';
import { ActionTop } from '../models/ActionTop';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-web-app',
  templateUrl: './web-app.component.html',
  styleUrls: ['./web-app.component.css']
})
export class WebAppComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  listActionTop: Array<ActionTop> = [
    {
      name : "Create",
      active : true
    },
    {
      name : "Connect",
      active : false
    },
    {
      name : "Share",
      active : false
    },
    {
      name : "Results",
      active : false
    },
  ];

  

  nameWorkspace: String = "Ma première story";
  nameUser: String = "Antoine";

}
