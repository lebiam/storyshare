import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DisplayStoryshareController } from './display-storyshare/display-storyshare.controller';
import { DisplayStoryshareService } from './display-storyshare/display-storyshare.service';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [AuthModule],
  controllers: [AppController, DisplayStoryshareController],
  providers: [AppService, DisplayStoryshareService],
})
export class AppModule {}
