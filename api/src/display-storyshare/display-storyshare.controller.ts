import { DisplayStoryshareService } from './display-storyshare.service';
import { Controller, Get, Req, Param, UseGuards } from '@nestjs/common';
import { DisplayStoryDto } from './dto/display-storyshare.dto';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsGuard } from 'src/permissions.guard';
import { Permissions } from '../permissions.decorator';

@Controller('play')
export class DisplayStoryshareController {

    constructor(private readonly displayStoryService: DisplayStoryshareService) {}
    
    //@UseGuards(AuthGuard('jwt'), PermissionsGuard)
    @Get(':id')
    //@Permissions('read:play')
    find(@Param() params): DisplayStoryDto {
        return this.displayStoryService.findOne(params.id);
    }
}