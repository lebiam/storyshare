import { Workspace } from './../../../front/src/app/models/Workspace';
import { User } from './../../../front/src/app/models/User';
import { DisplayStoryDto } from './dto/display-storyshare.dto';
import { Injectable } from '@nestjs/common';
import { Type } from '../../../front/src/app/models/Type.enum';

@Injectable()
export class DisplayStoryshareService {

    user: User = {
        id:78,
        name: 'Blueland',
        img: '../../../../assets/logo.png'
    }

    workSpace: Workspace = {
        id : 4545,
        name: 'Ma première story',
        stories: 
            [
                {
                  id: 456,
                  source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
                  type : Type.Video,
                  duration : 6000,
                  progression: 0
                },
                {
                  id: 456,
                  source : "https://picsum.photos/id/124/1080/1920",
                  type : Type.Image,
                  duration : 3000,
                  progression: 0
                },
                {
                  id: 456,
                  source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
                  type : Type.Video,
                  duration : 6000,
                  progression: 0
                },
                {
                  id: 456,
                  source : "https://picsum.photos/id/124/1080/1920",
                  type : Type.Image,
                  duration : 3000,
                  progression: 0
                },
                {
                  id: 456,
                  source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
                  type : Type.Video,
                  duration : 6000,
                  progression: 0
                },
                {
                  id: 456,
                  source : "https://picsum.photos/id/124/1080/1920",
                  type : Type.Image,
                  duration : 3000,
                  progression: 0
                },
                {
                  id: 456,
                  source : "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
                  type : Type.Video,
                  duration : 6000,
                  progression: 0
                },
                {
                  id: 456,
                  source : "https://picsum.photos/id/124/1080/1920",
                  type : Type.Image,
                  duration : 3000,
                  progression: 0
                }
              ]
        }
    

    findOne(id: number): DisplayStoryDto {
        var displayStory: DisplayStoryDto = {
            id: 457,
            workspace: this.workSpace,
            user: this.user
        }
        return displayStory;
    }
}
