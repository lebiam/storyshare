import { Workspace } from './../../../../front/src/app/models/Workspace';
import { User } from './../../../../front/src/app/models/User';
export class DisplayStoryDto {
    id: number;
    user: User;
    workspace: Workspace;
}
